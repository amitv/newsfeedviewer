## Display data from JSON feed without blocking main thread
`MasterViewController` populate data from JSON feed and lazily download images. Images will only be downloaded for the visible rows in `UITableView`

In 'viewDidLoad' method of 'MasterViewController' we initiate 'NSURLConnection' using,

    + (void)sendAsynchronousRequest:(NSURLRequest *)request queue:(NSOperationQueue *)queue completionHandler:(void (^)(NSURLResponse*, NSData*, NSError*))handler

Once we receive data we create `NSArray` of `Article` objects and reload our `UITableView`.

`UITableView` creates new instance of `ThumbnailDownloader` for each non null`thumbnailImageHref`. `MasterViewController` maintain `NSMutableDictionary` to avoid recreating new instance for same row while the download is still haven't finished for that row. The `key` of the dictionary is `indexPath`. Once the download finish we remove that from `dictionary` to avoid memory usage spikes.

`ThumbnailDownloader` uses `NSURLConnection` delegates to download thumbnail images.

## Future Improvements

- Pull to refresh with date set to last refresh date
- Better way to show `slugLine` when image exist for article. If we target only iOS7 then we can use TextKit to show image and text where text use full width when `UIImageView` bounds end.
- Custom view to show `webHref` with navigation bar hidden initially and appears when tapped.
- Add image caching in `ThumbnailDownloader`
- Background fetch in iOS7
- Offline reading
- Custom alert for displaying errors or meessage to users.

## Support iOS 6.0 or higher

- I dropped support for iOS 5.0 because Mavericks doesn't allow testing on iOS simulator 5.0.

## Others

- I didn't add dateLine info in the cell because I think it's waste of screen space. I think we should have updatedAt in the response and that can be added to cell. 