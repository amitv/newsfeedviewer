//
//  ArticleCell.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "ArticleCell.h"
#import "Article.h"

@interface ArticleCell()

@property (retain, nonatomic) Article *article;

@end

@implementation ArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        self.textLabel.font = [ABHelper headLineFont];
        self.textLabel.textColor = [UIColor colorWithRed:0.13 green:0.23 blue:0.44 alpha:1.0];
        self.textLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.textLabel.numberOfLines = 0;
        
        self.detailTextLabel.font = [ABHelper slugLineFont];
        self.detailTextLabel.numberOfLines = 0;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.textLabel setFrame:CGRectMake(kAB_LeftPaddingInCell,
                                        kAB_TopPaddingInCell,
                                        kAB_CellContentViewWidthWithoutImage(self.width),
                                        [self.article.headLine ABHeightForFont:[ABHelper headLineFont]
                                                                      andWidth:kAB_CellContentViewWidthWithoutImage(self.width)])];
    
    CGFloat yPos = self.textLabel.y + self.textLabel.height;
    
    if([self.article.thumbnailImageHref isKindOfClass:[NSNull class]]) {
        
        [self.detailTextLabel setFrame:CGRectMake(kAB_LeftPaddingInCell,
                                                  yPos,
                                                  kAB_CellContentViewWidthWithoutImage(self.width),
                                                  [self.article.slugLine ABHeightForFont:[ABHelper slugLineFont]
                                                                                andWidth:kAB_CellContentViewWidthWithoutImage(self.width)])];
        self.imageView.frame = CGRectZero;
        
    } else {
        
        [self.detailTextLabel setFrame:CGRectMake(kAB_LeftPaddingInCell,
                                                  yPos,
                                                  kAB_CellContentViewWidthWithImage(self.width),
                                                  [self.article.slugLine ABHeightForFont:[ABHelper slugLineFont]
                                                                                andWidth:kAB_CellContentViewWidthWithImage(self.width)])];
        
        self.imageView.frame = CGRectMake(self.detailTextLabel.x + self.detailTextLabel.width + 5.f, yPos, kAB_ThumbnailImageWidth, kAB_ThumbnailImageHeight);
    }
}

- (void)setDataFromArticle:(Article *)article
{
    self.article = article;
    self.textLabel.text = self.article.headLine;
    self.detailTextLabel.text = self.article.slugLine;
}

- (void)dealloc
{
    [_article release];
    [super dealloc];
}
@end
