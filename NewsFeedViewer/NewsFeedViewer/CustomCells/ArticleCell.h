//
//  ArticleCell.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;

@interface ArticleCell : UITableViewCell

/**
 Set text and image for the cell
 @param article A Article type object that contains data for the cell.
 
 @return nil
 */
- (void)setDataFromArticle:(Article *)article;

@end
