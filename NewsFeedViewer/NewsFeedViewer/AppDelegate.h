//
//  AppDelegate.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 13/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABNavigationController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (retain, nonatomic) UIWindow                  *window;
@property (retain, nonatomic) ABNavigationController    *navigationController;
@end
