//
//  ABNavigationController.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABNavigationController : UINavigationController

@end
