//
//  NSString+AB.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AB)

/**
 Returns height needed for drawing a string in the multi line label.
 
 @param aFont UIFont that will be used for drawing string
 @param aWidth Width of a label
 
 @return calculated size from width and font
 */
- (CGFloat)ABHeightForFont:(UIFont *)aFont andWidth:(CGFloat)aWidth;
@end
