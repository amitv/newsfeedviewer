//
//  UIView+vadr.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (AB)

- (CGPoint)origin;
- (void)setOrigin:(CGPoint)newOrigin;

- (CGSize)size;
- (void)setSize:(CGSize)newSize;

- (CGFloat)bottom;
- (CGFloat)right;

- (CGFloat)x;
- (void)setX:(CGFloat)newX;

- (CGFloat)y;
- (void)setY:(CGFloat)newY;

- (CGFloat)height;
- (void)setHeight:(CGFloat)newHeight;

- (CGFloat)width;
- (void)setWidth:(CGFloat)newWidth;

@end
