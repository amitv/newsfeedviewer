//
//  UIImage+AB.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "UIImage+AB.h"

@implementation UIImage (AB)

- (UIImage *)createThumbnailOfSize:(CGSize)thumbnailSize
{
    if (self.size.width != thumbnailSize.width || self.size.height != thumbnailSize.height) {
        UIGraphicsBeginImageContextWithOptions(thumbnailSize, NO, 0.0f);
        CGRect imageRect = CGRectMake(0.0, 0.0, thumbnailSize.width, thumbnailSize.height);
        [self drawInRect:imageRect];
        UIImage *anImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return anImage;
    } else {
        return self;
    }
}
@end
