//
//  NSString+AB.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "NSString+AB.h"

@implementation NSString (AB)

- (CGFloat)ABHeightForFont:(UIFont *)aFont andWidth:(CGFloat)aWidth
{
    CGSize maximumLabelSize = CGSizeMake(aWidth, 1000.f);
    return [self sizeWithFont:aFont constrainedToSize:maximumLabelSize].height;
}
@end
