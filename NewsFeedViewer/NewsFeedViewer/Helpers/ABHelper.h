//
//  AppUtils.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABHelper : NSObject

/**
 Show alert box with only "OK" button.
 
 @param message A message that needs to be displayed in the alert box.
 @return nil
 */
+ (void)showSimpleAlertForMessage:(NSString *)message;

/**
 Returns application font.
 
 @param size Size of the font.
 @param bold Boolean value for bold type font.
 
 @return UIFont to use
 */
+ (UIFont *)applicationFontOfSize:(CGFloat)size isBold:(BOOL)bold;

/**
 Returns font for headline
 */
+ (UIFont *)headLineFont;

/**
 Returns font for slugline
 */
+ (UIFont *)slugLineFont;

/**
 Returns font for dateline
 */
+ (UIFont *)dateLineFont;

@end
