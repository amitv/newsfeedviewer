//
//  ABCommon.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#ifndef NewsFeedViewer_ABCommon_h
#define NewsFeedViewer_ABCommon_h

//Application name
static NSString * const kAB_ApplicationName = @"NewsFeedViewer";

// Feed URL
static NSString * const APP_FEED_URL   =  @"http://mobilatr.mob.f2.com.au/services/views/9.json";

// Keys in JSON response
static NSString * const KEY_NAME               = @"name";
static NSString * const KEY_ITEMS              = @"items";

#define kAB_ScreenWidth                  [[UIScreen mainScreen] applicationFrame].size.width
#define kAB_ScreenHeight                 [[UIScreen mainScreen] applicationFrame].size.height

#define kAB_ThumbnailImageWidth    90.f
#define kAB_ThumbnailImageHeight   60.f

#define kAB_LeftPaddingInCell       7.f
#define kAB_TopPaddingInCell        7.f
#define kAB_BottomPaddingInCell     7.f
#define kAB_DetailDiscolsureWidth   30.f

#define kAB_CellContentViewWidthWithoutImage(width)        width - (kAB_LeftPaddingInCell * 2) - kAB_DetailDiscolsureWidth
#define kAB_CellContentViewWidthWithImage(width)           width - (kAB_LeftPaddingInCell * 2) - kAB_DetailDiscolsureWidth - kAB_ThumbnailImageWidth
#endif
