//
//  ThumbnailDownloader.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "ABThumbnailDownloader.h"
#import "Article.h"

@interface ABThumbnailDownloader ()
@property (retain, nonatomic) NSMutableData     *activeDownload;
@property (retain, nonatomic) NSURLConnection   *imageConnection;
@end

@implementation ABThumbnailDownloader

#pragma mark

- (void)startDownload
{
    self.activeDownload = [NSMutableData data];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.article.thumbnailImageHref]];
    
    // alloc+init and start an NSURLConnection; release on completion/failure
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.imageConnection = conn;
    [conn release];
}

- (void)cancelDownload
{
    [self.imageConnection cancel];
    self.imageConnection = nil;
    self.activeDownload = nil;
}

- (void)dealloc
{
    [_article release];
    [_activeDownload release];
    [_imageConnection release];
    [_completionHandler release];
    [super dealloc];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.activeDownload appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	// Clear the activeDownload property to allow later attempts
    self.activeDownload = nil;
    
    // Release the connection now that it's finished
    self.imageConnection = nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Set appIcon and clear temporary data/image
    UIImage *image = [[UIImage alloc] initWithData:self.activeDownload];
    self.article.thumbnailImage = [image createThumbnailOfSize:CGSizeMake(kAB_ThumbnailImageWidth, kAB_ThumbnailImageHeight)];
    [image release];
    
    // Release data
    self.activeDownload = nil;
    
    // Release the connection now that it's finished
    self.imageConnection = nil;
    
    if (self.completionHandler)
        self.completionHandler();
}
@end
