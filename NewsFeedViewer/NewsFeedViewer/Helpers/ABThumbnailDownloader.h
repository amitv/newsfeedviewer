//
//  ThumbnailDownloader.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Article;
@interface ABThumbnailDownloader : NSObject

@property (retain, nonatomic) Article *article;
@property (copy, nonatomic) void (^completionHandler)(void);

/**
 Start downloading image
 */
- (void)startDownload;

/**
 Cancel downloading image
 */
- (void)cancelDownload;

@end
