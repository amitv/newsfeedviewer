//
//  AppUtils.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "ABHelper.h"

@implementation ABHelper

+ (void)showSimpleAlertForMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:kAB_ApplicationName message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}

+ (UIFont *)applicationFontOfSize:(CGFloat)size isBold:(BOOL)bold
{
    if(bold)
        return [UIFont fontWithName:@"HelveticaNeue-Bold" size:size];
    else
        return [UIFont fontWithName:@"HelveticaNeue" size:size];
}

+ (UIFont *)headLineFont
{
    return [ABHelper applicationFontOfSize:14.f isBold:YES];
}

+ (UIFont *)slugLineFont
{
    return [ABHelper applicationFontOfSize:12.f isBold:NO];
}

+ (UIFont *)dateLineFont
{
    return [ABHelper applicationFontOfSize:11.f isBold:NO];
}
@end
