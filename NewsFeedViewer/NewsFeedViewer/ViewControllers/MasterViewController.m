//
//  MasterViewController.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 13/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "MasterViewController.h"
#import "ArticleCell.h"
#import "Article.h"
#import "ABThumbnailDownloader.h"
#import "DetailViewController.h"

@interface MasterViewController () <UIScrollViewDelegate>
@property (retain, nonatomic) UITableView           *tableView;
@property (retain, nonatomic) NSArray               *feedData;
@property (retain, nonatomic) NSMutableDictionary   *thumnailDownloadsInProgress;
@property (assign, nonatomic) NSInteger              lastOrientation;
@property (assign, nonatomic) NSInteger              currentOrientation;
@end

@implementation MasterViewController

@synthesize tableView = _tableView;

- (id)init
{
    self = [super init];
    if(self) {
        self.feedData = [[[NSArray alloc] init] autorelease];
        self.thumnailDownloadsInProgress = [[[NSMutableDictionary alloc] init] autorelease];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    
    // We are saving last orientation state here to avoid
    // recalculating row heights on every table reload.
    self.lastOrientation = [UIDevice currentDevice].orientation;
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
    
    // Initialising UITableView and adding to view
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectZero
                                                   style:UITableViewStylePlain] autorelease];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    [self fetchFeedData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableView.frame = self.view.bounds;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // terminate all pending download connections
    NSArray *allDownloads = [self.thumnailDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
    
    [self.thumnailDownloadsInProgress removeAllObjects];
}

- (void)dealloc
{
    //    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [_feedData release];
    [_thumnailDownloadsInProgress release];
    [_tableView release];
    [super dealloc];
}

#pragma makr - Orientation observer
- (void)orientationChanged:(NSNotification *)notitfication
{
    self.currentOrientation = [UIDevice currentDevice].orientation;
    [self.tableView reloadData];
}

#pragma mark - Fech feed data
// Fetch data from feed URL
- (void)fetchFeedData
{
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:APP_FEED_URL]];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    __block MasterViewController *weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:urlRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                               
                               // To refresh the feed
                               self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                                                                       target:self
                                                                                                                       action:@selector(fetchFeedData)] autorelease];
                               
                               if(error == nil) {
                                   NSError *jsonError = nil;
                                   // Serialising JSON data
                                   id responseData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
                                   NSAssert([responseData isKindOfClass:[NSDictionary class]], @"Expecting NSDictionary but got some other type");
                                   if(jsonError == nil) {
                                       
                                       // Set navigation bar title
                                       weakSelf.title = [responseData valueForKey:KEY_NAME];
                                       
                                       // Get all articles
                                       id itemArray = [responseData valueForKey:KEY_ITEMS];
                                       
                                       NSAssert([itemArray isKindOfClass:[NSArray class]], @"Expecting NSArray but got some other type");
                                       
                                       // create Article object for all articles.
                                       NSMutableArray *articlesArray = [[NSMutableArray alloc] initWithCapacity:[itemArray count]];
                                       
                                       for (NSDictionary *aDictionary in itemArray) {
                                           Article *article = [[Article alloc] init];
                                           [aDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                                               [article setValue:obj forKey:key];
                                           }];
                                           [articlesArray addObject:article];
                                           [article release];
                                       }
                                       weakSelf.feedData = [NSArray arrayWithArray:articlesArray];
                                       [articlesArray release];
                                       [weakSelf.tableView reloadData];
                                   } else {
                                       // If NSJSONSerialization fails to convert JSON to
                                       // Foundation objects.
                                       [ABHelper showSimpleAlertForMessage:@"Oops! Something went wrong."];
                                   }
                               } else {
                                   // If NSURLConnection fails to complete successfully.
                                  if([error code] == NSURLErrorCannotFindHost) {
                                       [ABHelper showSimpleAlertForMessage:@"Damn gerbils have stopped running again! Someone has been dispatched to poke them with a sharp stick."];
                                  } else {
                                      [ABHelper showSimpleAlertForMessage:[error localizedDescription]];
                                  }
                               }
                           }];
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.feedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    ArticleCell *articleCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(articleCell == nil) {
        articleCell = [[[ArticleCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier] autorelease];
        articleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    Article *article = [self.feedData objectAtIndex:indexPath.row];
    [articleCell setDataFromArticle:article];
    
    // Set thumbnail Image
    // Only load cached images; defer new downloads until scrolling ends
    if(article.thumbnailImage) {
        
        articleCell.imageView.image = article.thumbnailImage;
    } else {
        articleCell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        if (self.tableView.dragging == NO && self.tableView.decelerating == NO) {
            
            [self startThumbnailDownload:article forIndexPath:indexPath];
        }
        // if a download is deferred or in progress, return a placeholder image
    }
    return articleCell;
}


#pragma mark - UITableView Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Article *article = [self.feedData objectAtIndex:indexPath.row];
    CGFloat rowHeight = article.rowHeight;
    if(rowHeight == 0 || (self.lastOrientation != self.currentOrientation)) {
        
        // Calculate height for Headline text
        CGFloat headLineHeight = [article.headLine ABHeightForFont:[ABHelper headLineFont]
                                                          andWidth:kAB_CellContentViewWidthWithoutImage(self.view.width)];
        
        // Calculate height for Slugline text
        CGFloat slugLineHeight;
        if([article.thumbnailImageHref isKindOfClass:[NSNull class]]) {
            slugLineHeight = [article.slugLine ABHeightForFont:[ABHelper slugLineFont]
                                                      andWidth:kAB_CellContentViewWidthWithoutImage(self.view.width)];
            
        } else {
            slugLineHeight = [article.slugLine ABHeightForFont:[ABHelper slugLineFont]
                                                      andWidth:kAB_CellContentViewWidthWithImage(self.view.width)];
            if(slugLineHeight < kAB_ThumbnailImageHeight)
                slugLineHeight = kAB_ThumbnailImageHeight;
        }
        
        // Calculate total height needed for the row
        rowHeight = headLineHeight + slugLineHeight + kAB_TopPaddingInCell + kAB_BottomPaddingInCell;
        article.rowHeight = rowHeight;
    }
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Article *article = [self.feedData objectAtIndex:indexPath.row];
    if([article.webHref isKindOfClass:[NSNull class]])
        return;
    
    // Open the Webview for the selected row.
    DetailViewController *detailViewController = [[[DetailViewController alloc] init] autorelease];
    [detailViewController openWebViewWithURL:[NSURL URLWithString:article.webHref]];
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - Table cell image support
- (void)startThumbnailDownload:(Article *)article forIndexPath:(NSIndexPath *)indexPath
{
    if([article.thumbnailImageHref isKindOfClass:[NSNull class]])
        return;
    
    ABThumbnailDownloader *thumbnailDownloader = [self.thumnailDownloadsInProgress objectForKey:indexPath];
    __block MasterViewController *weakSelf = self;
    if (thumbnailDownloader == nil) {
        
        thumbnailDownloader = [[ABThumbnailDownloader alloc] init];
        thumbnailDownloader.article = article;
        [thumbnailDownloader setCompletionHandler:^{
            
            ArticleCell *cell = (ArticleCell *)[weakSelf.tableView cellForRowAtIndexPath:indexPath];
            
            // Display the newly loaded image
            cell.imageView.image = article.thumbnailImage;
            
            // Remove the ThumbnailDownloader from the in progress list.
            // This will result in it being deallocated.
            [weakSelf.thumnailDownloadsInProgress removeObjectForKey:indexPath];
        }];
        [self.thumnailDownloadsInProgress setObject:thumbnailDownloader forKey:indexPath];
        [thumbnailDownloader startDownload];
        [thumbnailDownloader release];
    }
}

//  This method is used in case the user scrolled into a set of cells that don't
//  have their thumbnails yet.
- (void)loadImagesForOnscreenRows
{
    if ([self.feedData count] > 0) {
        
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths) {
            Article *article = [self.feedData objectAtIndex:indexPath.row];
            
            if (article.thumbnailImage == nil) {
                [self startThumbnailDownload:article forIndexPath:indexPath];
            }
        }
    }
}
//
//#pragma mark - UIScrollViewDelegate
////  Load images for all onscreen rows when scrolling is finished.
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (decelerate == NO)
	{
        [self loadImagesForOnscreenRows];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

@end
