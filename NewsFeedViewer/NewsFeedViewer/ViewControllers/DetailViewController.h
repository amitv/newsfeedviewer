//
//  DetailViewController.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 13/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIWebViewDelegate>

/**
 Open webview.
 
 @param webUrl A URL to open.
 
 @return nil
 */
- (void)openWebViewWithURL:(NSURL *)webUrl;
@end
