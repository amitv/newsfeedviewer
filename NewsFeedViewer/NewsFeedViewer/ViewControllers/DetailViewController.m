//
//  DetailViewController.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 13/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()
@property (nonatomic, retain) UIWebView *webView;
@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.webView.frame = self.view.bounds;
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_webView release];
    [super dealloc];
}

#pragma mark -
- (void)openWebViewWithURL:(NSURL *)webUrl
{
    if(self.webView == nil) {
        // Initialise UIWebView and add it to the view
        self.webView = [[[UIWebView alloc] initWithFrame:CGRectZero] autorelease];
        self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.webView.delegate = self;
        [self.view addSubview:self.webView];
    }
    [self.webView loadRequest:[NSURLRequest requestWithURL:webUrl]];
}

#pragma mark - UIWebView Delegates
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
@end
