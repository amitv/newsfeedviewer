//
//  Articles.h
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property (retain, nonatomic) NSString      *dateLine;
@property (retain, nonatomic) NSString      *headLine;
@property (retain, nonatomic) NSString      *identifier;
@property (retain, nonatomic) NSString      *slugLine;
@property (retain, nonatomic) id            thumbnailImageHref;
@property (retain, nonatomic) NSString      *tinyUrl;
@property (retain, nonatomic) NSString      *type;
@property (retain, nonatomic) NSString      *webHref;
@property (retain, nonatomic) UIImage       *thumbnailImage;
@property (assign, nonatomic) CGFloat       rowHeight;
@end
