//
//  Articles.m
//  NewsFeedViewer
//
//  Created by Amit Vaghela on 15/12/2013.
//  Copyright (c) 2013 Amit Vaghela. All rights reserved.
//

#import "Article.h"

@implementation Article

- (void)dealloc
{
    [_dateLine release];
    [_headLine release];
    [_identifier release];
    [_slugLine release];
    [_thumbnailImageHref release];
    [_tinyUrl release];
    [_type release];
    [_webHref release];
    [_thumbnailImage release];
    [super dealloc];
}
@end
